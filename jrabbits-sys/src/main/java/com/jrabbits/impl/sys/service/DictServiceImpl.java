package com.jrabbits.impl.sys.service;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jrabbits.api.sys.entity.Dict;
import com.jrabbits.api.sys.service.DictService;
import com.jrabbits.common.persistence.Page;
import com.jrabbits.impl.sys.dao.DictDao;

/**
 * 字典Service
 * @author jrabbits
 * @version 2014-05-16
 */

@Transactional(readOnly = true)
@Service("DictService")
public class DictServiceImpl implements DictService {
	
	@Autowired
	private DictDao dictDao;
	/**
	 * 查询字段类型列表
	 * @return
	 */
	public List<String> findTypeList(){
		return dictDao.findTypeList(new Dict());
	}

	@Transactional(readOnly = false)
	public void save(Dict dict) {
		if (dict.getIsNewRecord()){
			dictDao.insert(dict);
		}else{
			dictDao.update(dict);
		}
	}

	@Transactional(readOnly = false)
	public void delete(Dict dict) {
		dictDao.delete(dict);
	}

	@Override
	public Dict get(String id) {
		
		return dictDao.get(id);
	}

	@Override
	public List<Dict> findList(Dict dict) {
		if(dict != null){
			dictDao.findList(dict);
		}
		return  new ArrayList<Dict>();
	}

	@Override
	public Page<Dict> findPage(Page<Dict> page, Dict dict) {
		dict.setPage(page);
		page.setList(dictDao.findList(dict));
		return page;
	}

	@Override
	public List<Dict> findAllList(Dict dict) {
		// TODO Auto-generated method stub
		return dictDao.findAllList(dict);
	}

}
