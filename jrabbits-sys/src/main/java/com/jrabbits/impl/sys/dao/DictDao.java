package com.jrabbits.impl.sys.dao;

import java.util.List;

import com.jrabbits.api.sys.entity.Dict;
import com.jrabbits.common.persistence.CrudDao;
import com.jrabbits.common.persistence.annotation.MyBatisDao;

/**
 * 字典DAO接口
 * @author jrabbits
 * @version 2014-05-16
 */
@MyBatisDao
public interface DictDao extends CrudDao<Dict> {

	public List<String> findTypeList(Dict dict);
	
}
