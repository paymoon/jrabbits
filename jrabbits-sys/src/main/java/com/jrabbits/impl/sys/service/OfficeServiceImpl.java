package com.jrabbits.impl.sys.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jrabbits.api.sys.entity.Office;
import com.jrabbits.api.sys.service.OfficeService;
import com.jrabbits.common.persistence.Page;
import com.jrabbits.common.service.TreeService;
import com.jrabbits.impl.sys.dao.OfficeDao;

/**
 * 机构Service
 * @author jrabbits
 * @version 2014-05-16
 */

@Transactional(readOnly = true)
@Service("OfficeService")
public class OfficeServiceImpl extends TreeService<OfficeDao, Office>  implements OfficeService{
	
	@Autowired 
	private OfficeDao officeDao;
	/**
	 * 获取单条数据
	 * @param id
	 * @return
	 */
	public Office get(String id) {
		return officeDao.get(id);
	}
	
	/**
	 * 获取单条数据
	 * @param entity
	 * @return
	 */
	public Office get(Office office) {
		return officeDao.get(office);
	}
	@Transactional(readOnly = false)
	public void save(Office office) {
		super.save(office);
	}
	/**
	 * 保存数据（插入或更新）
	 * @param entity
	 */
	@Transactional(readOnly = false)
	public void saveOffice(Office office) {
		if (office.getIsNewRecord()){
			officeDao.insert(office);
		}else{
			officeDao.update(office);
		}
	}
	/**
	 * 删除数据
	 * @param entity
	 */
	@Transactional(readOnly = false)
	public void delete(Office office) {
		officeDao.delete(office);
	}
	/**
	 * 查询列表数据
	 * @param entity
	 * @return
	 */
	@Transactional(readOnly = true)
	public List<Office> findList(Office office){
		if(office != null){
			office.setParentIds(office.getParentIds()+"%");
			return officeDao.findByParentIdsLike(office);
		}
		return  new ArrayList<Office>();
	}
	/**
	 * 查询分页数据
	 * @param page 分页对象
	 * @param entity
	 * @return
	 */
	public Page<Office> findPage(Page<Office> page,Office office) {
		office.setPage(page);
		page.setList(officeDao.findList(office));
		return page;
	}

//	public List<Office> findAll(){
//		return UserUtils.getOfficeList();
//	}
//	
//	public List<Office> findList(Boolean isAll){
//		if (isAll != null && isAll){
//			return UserUtils.getOfficeAllList();
//		}else{
//			return UserUtils.getOfficeList();
//		}
//	}

	@Override
	public List<Office> findAllList(Office office) {
		// TODO Auto-generated method stub
		return officeDao.findAllList(office);
	}
	
}
