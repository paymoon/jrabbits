package com.jrabbits.impl.sys.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jrabbits.api.sys.entity.Menu;
import com.jrabbits.api.sys.service.MenuService;
import com.jrabbits.common.utils.StringUtils;
import com.jrabbits.impl.sys.dao.MenuDao;

@Service("MenuService")
@Transactional(readOnly = true)
public class MenuServiceImpl implements MenuService {

	@Autowired
	private MenuDao menuDao;
	
	public Menu getMenu(String id) {
		return menuDao.get(id);
	}
	public List<Menu> findAllList(Menu menu) {
		
		return menuDao.findAllList(menu);
	}
	public List<Menu> findByUserId(Menu menu) {
		
		return menuDao.findByUserId(menu);
	}

//	public List<Menu> findAllMenu(){
//		return UserUtils.getMenuList();
//	}
	
	@Transactional(readOnly = false)
	public void saveMenu(Menu menu) {
		
		// 获取父节点实体
		menu.setParent(this.getMenu(menu.getParent().getId()));
		
		// 获取修改前的parentIds，用于更新子节点的parentIds
		String oldParentIds = menu.getParentIds(); 
		
		// 设置新的父节点串
		menu.setParentIds(menu.getParent().getParentIds()+menu.getParent().getId()+",");

		// 保存或更新实体
		if (StringUtils.isBlank(menu.getId())){
			//menu.preInsert();
			menuDao.insert(menu);
		}else{
			//menu.preUpdate();
			menuDao.update(menu);
		}
		
		// 更新子节点 parentIds
		Menu m = new Menu();
		m.setParentIds("%,"+menu.getId()+",%");
		List<Menu> list = menuDao.findByParentIdsLike(m);
		for (Menu e : list){
			e.setParentIds(e.getParentIds().replace(oldParentIds, menu.getParentIds()));
			menuDao.updateParentIds(e);
		}
	}

	@Transactional(readOnly = false)
	public void updateMenuSort(Menu menu) {
		menuDao.updateSort(menu);
	}

	@Transactional(readOnly = false)
	public void deleteMenu(Menu menu) {
		menuDao.delete(menu);
	}
}
