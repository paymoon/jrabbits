package com.jrabbits.impl.sys.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jrabbits.api.sys.entity.User;
import com.jrabbits.api.sys.service.UserService;
import com.jrabbits.impl.sys.dao.UserDao;

@Transactional(readOnly = true)
@Service("UserService")
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDao userDao;
	
	public User get(String id){
		return userDao.get(id);
	}
	
	public User getByLoginName(User user) {
		return userDao.getByLoginName(user);
	}
	public void delete(User user){
		userDao.delete(user);
	}
}
