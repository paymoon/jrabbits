package com.jrabbits.impl.sys.dao;

import com.jrabbits.api.sys.entity.Log;
import com.jrabbits.common.persistence.CrudDao;
import com.jrabbits.common.persistence.annotation.MyBatisDao;

/**
 * 日志DAO接口
 * @author jrabbits
 * @version 2014-05-16
 */
@MyBatisDao
public interface LogDao extends CrudDao<Log> {

}
