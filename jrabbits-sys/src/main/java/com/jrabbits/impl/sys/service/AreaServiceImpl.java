package com.jrabbits.impl.sys.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jrabbits.api.sys.entity.Area;
import com.jrabbits.api.sys.service.AreaService;
import com.jrabbits.common.service.TreeService;
import com.jrabbits.impl.sys.dao.AreaDao;

/**
 * 区域Service
 * @author jrabbits
 * @version 2014-05-16
 */
@Service("AreaService")
@Transactional(readOnly = true)
public class AreaServiceImpl extends TreeService<AreaDao, Area>  implements AreaService {
	@Autowired
	private AreaDao areaDao;
	public List<Area> findAll(){
		return areaDao.findAllList(new Area());
	}

	@Transactional(readOnly = false)
	public void save(Area area) {
		super.save(area);
	}
	
	@Transactional(readOnly = false)
	public void delete(Area area) {
		super.delete(area);
	}
	
	/**
	 * 预留接口，用户更新子节前调用
	 * @param childEntity
	 */
	protected void preUpdateChild(Area entity, Area childEntity) {
		
	}

	@Override
	public Area get(String id) {
		return super.get(id);
	}

	@Override
	public List<Area> findAllList(Area area) {
		
		return areaDao.findAllList(area);
	}
}
