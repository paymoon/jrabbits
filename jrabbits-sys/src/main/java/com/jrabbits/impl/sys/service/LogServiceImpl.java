package com.jrabbits.impl.sys.service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jrabbits.api.sys.entity.Log;
import com.jrabbits.api.sys.service.LogService;
import com.jrabbits.common.persistence.Page;
import com.jrabbits.common.utils.DateUtils;
import com.jrabbits.impl.sys.dao.LogDao;

/**
 * 日志Service
 * @author jrabbits
 * @version 2014-05-16
 */
@Transactional(readOnly = true)
@Service("LogService")
public class LogServiceImpl implements LogService {

	@Autowired
	private LogDao logDao;
	
	public Page<Log> findPage(Page<Log> page, Log log) {
		
		// 设置默认时间范围，默认当前月
		if (log.getBeginDate() == null){
			log.setBeginDate(DateUtils.setDays(DateUtils.parseDate(DateUtils.getDate()), 1));
		}
		if (log.getEndDate() == null){
			log.setEndDate(DateUtils.addMonths(log.getBeginDate(), 1));
		}
		log.setPage(page);
		page.setList(logDao.findList(log));
		return page;
	}
	@Transactional(readOnly = false)
	public void insert(Log log) {
		
		logDao.insert(log);
	}
}
