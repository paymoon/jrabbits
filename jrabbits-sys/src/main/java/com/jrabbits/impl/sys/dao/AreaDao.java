package com.jrabbits.impl.sys.dao;

import com.jrabbits.api.sys.entity.Area;
import com.jrabbits.common.persistence.TreeDao;
import com.jrabbits.common.persistence.annotation.MyBatisDao;

/**
 * 区域DAO接口
 * @author jrabbits
 * @version 2014-05-16
 */
@MyBatisDao
public interface AreaDao extends TreeDao<Area> {
	
}
