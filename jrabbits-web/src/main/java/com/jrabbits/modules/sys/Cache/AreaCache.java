package com.jrabbits.modules.sys.Cache;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jrabbits.api.sys.entity.Area;
import com.jrabbits.api.sys.service.AreaService;
import com.jrabbits.api.sys.utils.UserUtils;
import com.jrabbits.common.constant.CacheDict;
@Service("AreaCache")
@Transactional(readOnly = true)
public class AreaCache {
	
	@Autowired
	private AreaService areaService;
	public List<Area> findAll(){
		return UserUtils.getAreaList();
	}

	public void save(Area area) {
		areaService.save(area);
		UserUtils.removeCache(CacheDict.CACHE_AREA_LIST);
	}
	
	public void delete(Area area) {
		areaService.delete(area);
		UserUtils.removeCache(CacheDict.CACHE_AREA_LIST);
	}
}
