package com.jrabbits.modules.sys.impl;

import org.springframework.stereotype.Service;

import com.jrabbits.api.sys.entity.User;
import com.jrabbits.api.sys.service.CurrentUserSevice;
import com.jrabbits.api.sys.utils.UserUtils;

@Service("CurrentUserSevice")
public class CurrentUserServiceImpl implements CurrentUserSevice {

	public User getCurrentUser() {
		return UserUtils.getUser();
	}

}
