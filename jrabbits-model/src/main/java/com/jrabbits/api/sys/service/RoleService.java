package com.jrabbits.api.sys.service;

import java.util.List;

/**
 * 用户Service
 * @author jrabbits
 * @version 2014-05-16
 */


import com.jrabbits.api.sys.entity.Role;

public interface RoleService {
	public List<Role> findList(Role role);
	public List<Role> findAllList(Role role);
}
