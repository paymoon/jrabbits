package com.jrabbits.api.sys.service;

import java.util.Collection;
import java.util.List;

import org.apache.shiro.session.Session;

import com.jrabbits.api.sys.entity.Menu;
import com.jrabbits.api.sys.entity.Role;
import com.jrabbits.api.sys.entity.User;
import com.jrabbits.common.persistence.Page;

/**
 * 系统管理，安全相关实体的管理类,包括用户、角色、菜单.
 * @author jrabbits
 * @version 2013-12-05
 */
public interface SystemService {
	

	//-- User Service --//
	
	/**
	 * 获取用户
	 * @param id
	 * @return
	 */
	//public User getUser(String id);

	/**
	 * 根据登录名获取用户
	 * @param loginName
	 * @return
	 */
	//public User getUserByLoginName(String loginName);
	
	public Page<User> findUser(Page<User> page, User user);
	
	/**
	 * 无分页查询人员列表
	 * @param user
	 * @return
	 */
	public List<User> findUser(User user);

	/**
	 * 通过部门ID获取用户列表，仅返回用户id和name（树查询用户时用）
	 * @param user
	 * @return
	 */
	public List<User> findUserByOfficeId(String officeId);
	
	public void saveUser(User user);
	
	public void updateUserInfo(User user);
	
	public void deleteUser(User user);
	
	public void updatePasswordById(String id, String loginName, String newPassword);
	
	public void updateUserLoginInfo(User user);
	/**
	 * 获得活动会话
	 * @return
	 */
	public Collection<Session> getActiveSessions();
	/**
	 * 获取活动会话
	 * @param includeLeave 是否包括离线（最后访问时间大于3分钟为离线会话）
	 * @return
	 */
	public Collection<Session> getActiveSessions(boolean includeLeave);
	
	/**
	 * 获取活动会话
	 * @param includeLeave 是否包括离线（最后访问时间大于3分钟为离线会话）
	 * @param principal 根据登录者对象获取活动会话
	 * @param filterSession 不为空，则过滤掉（不包含）这个会话。
	 * @return
	 */
	public Collection<Session> getActiveSessions(boolean includeLeave, Object principal, Session filterSession);
	
	public void  deleteSession(Session session);
	
	//-- Role Service --//
	
	public Role getRole(String id);

	public Role getRoleByName(String name);
	
	public Role getRoleByEnname(String enname);
	
	public List<Role> findRole(Role role);
	
	//public List<Role> findAllRole();
	
	public void saveRole(Role role);

	public void deleteRole(Role role);
	
	public Boolean outUserInRole(Role role, User user);
	
	public User assignUserToRole(Role role, User user);

	
}
