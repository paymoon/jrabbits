package com.jrabbits.api.sys.service;

import com.jrabbits.api.sys.entity.Log;
import com.jrabbits.common.persistence.Page;

/**
 * 日志Service
 * @author jrabbits
 * @version 2014-05-16
 */
public interface LogService{

	public Page<Log> findPage(Page<Log> page, Log log);
	public void insert(Log log);
}
