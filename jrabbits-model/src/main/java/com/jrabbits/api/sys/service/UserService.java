package com.jrabbits.api.sys.service;

import com.jrabbits.api.sys.entity.User;

/**
 * 角色Service
 * @author jrabbits
 * @version 2014-05-16
 */
public interface UserService {
	public User get(String id);
	public void delete(User user);
	public User getByLoginName(User user);
}
