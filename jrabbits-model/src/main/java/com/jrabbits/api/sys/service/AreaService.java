package com.jrabbits.api.sys.service;

import java.util.List;

import com.jrabbits.api.sys.entity.Area;

/**
 * 区域Service
 * @author jrabbits
 * @version 2014-05-16
 */
public interface AreaService{
	
	public Area get(String id);
	
	public List<Area> findAll();
	
	public List<Area> findAllList(Area area);
	
	public void save(Area area);
	
	public void delete(Area area);
	
}
