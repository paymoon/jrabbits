package com.jrabbits.api.sys.service;

import java.util.List;

import com.jrabbits.api.sys.entity.Menu;

public interface MenuService {
	
	public Menu getMenu(String id);
	
	//public List<Menu> findAllMenu();
	
	public void saveMenu(Menu menu);

	public void updateMenuSort(Menu menu);

	public void deleteMenu(Menu menu);
	
	public List<Menu> findAllList(Menu menu);
	
	public List<Menu> findByUserId(Menu menu);
}
