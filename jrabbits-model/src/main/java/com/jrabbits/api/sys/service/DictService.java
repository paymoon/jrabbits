package com.jrabbits.api.sys.service;

import java.util.List;

import com.jrabbits.api.sys.entity.Dict;
import com.jrabbits.common.persistence.Page;

/**
 * 字典Service
 * @author jrabbits
 * @version 2014-05-16
 */
public interface DictService {
	
	/**
	 * 查询字段类型列表
	 * @return
	 */
	public List<String> findTypeList();
	
	public Dict get(String id);
	
	public void save(Dict dict);
	
	public List<Dict> findList(Dict dict);
	
	public List<Dict> findAllList(Dict dict);
	
	public Page<Dict> findPage(Page<Dict> page, Dict dict);
	
	public void delete(Dict dict);

}
